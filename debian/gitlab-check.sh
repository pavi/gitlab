#! /bin/sh

set -e

# Read debian specific configuration
. /etc/gitlab/gitlab-debian.conf
export DB RAILS_ENV

cd /usr/share/gitlab

# Check gitlab is configured correctly
printf "Check if Gitlab is configured correctly...\n"
runuser -u ${gitlab_user} -- sh -c 'bundle exec rake gitlab:check'
